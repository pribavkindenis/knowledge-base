# Графы

[<< Алгоритмы](./content.md)

## Разновидности графов

```mermaid
graph LR
1 --> 2;
1 --> 4;
2 --> 1;
2 --> 5;
2 --> 3;
3 --> 4;
3 --> 5;
4 --> 1;
5 --> 3;
5 --> 2;
```

> **Графом** называется упорядоченная пара множеств $`G = (V, E)`$, где $`V`$ - множество вершин, а $`E = V \times V`$ - упорядоченное или не упорядоченное множество рёбер, соединяющих пары вершин из $`V`$.

> **Петлёй (loop)** называется ребро, соединяющее только одну вершину $`(x, x) \in E`$

> Ребра называется **кратными (multiedge)**, если они соединяют одну и ту же пару вершин $`(x, y)`$

> **Степенью** вершины называется количество входящих в неё рёбер

Классификация графов:

* **Неориентированные  (undirected) / Ориентированные (directed)**: граф $`G = (V, E)`$ является неориентированным, если $`\forall (x,y) \in E \implies (y, x) \in E`$. В противном случае говорят, что граф ориентированный

* **Взвешенные (weighted) / Невзвешенные (unweighted)**: Каждому ребру взвешенного  графа $`G`$ присваивается числовое значение, или вес. В противном случае граф невзвешенный .

* **Простые (simple) / Сложные (multigraph)**: Простым  называется граф, не имеющий кратных рёбер и петель. В противном случае он назвается сложным

* **Разреженные (sparse) / Плотные (dense)**: Граф является разреженным , когда в дейвствительности рёбра определены только для малой части возможных пар вершин. В противном случае, граф называется плотным. Как правило, у плотных графов отношение кол-ва рёбер к кол-ву вершин описывается квадратичной функцией, а у разреженных - линейной

* **Циклические (cyclic) / Ациклические (acyclic)**: Ациклический граф не содержит циклов

* **Помеченные / Непомеченные**: Граф называется помеченным, если каждая его вершина имеет свою метку/идентификатор.

* Граф называется **Вложенным (embedded)**, если его вершинам и рёбрам присвоены геометрические позиции, при этом никакие рёбра не пересекаются друг с другом

* Граф называется **Полным (complete)**, если каждая его вершина соединена рёбрами со всеми остальными

## Способы программного задания графа

* Матрица смежности
* Списки смежности

```python
import random
from typing import List, Optional


class EdgeNode:
    def __init__(self,
                 vertex: int,
                 weight: int = None,
                 next_edge: 'EdgeNode' = None):
        self.vertex: int = vertex
        self.weight: int = weight
        self.next_edge: 'EdgeNode' = next_edge


class NodeGraph:
    def __init__(self,
                 vertices_size: int,
                 edges_size: int,
                 is_directed: bool = False,
                 is_weighted: bool = False):
        self.vertices_size: int = vertices_size
        self.edge_size: int = edges_size
        self.is_directed: bool = is_directed
        self.is_weighted: bool = is_weighted
        self.degree: List[int] = [0] * vertices_size
        self.edges: List[Optional[EdgeNode]] = [None] * vertices_size
```

## Обход в ширину  (BFS)

При обходе в ширину неориентированного графа каждому ребру присваивается направление от "открывающей" к открываемой вершине. В этом контексте вершина U называется родителем (parent) или предшественником (predcessor) вершини V, а вершина V - потомком вершины U.

Так как каждый узел, за исключением корня, имеет только одного родителя, получится **дерево вершин графа**, определяющее **кратчайший путь** от корня ко всем другим узлам.

Пример дерева кратчайших путей с вершиной 1: 

```mermaid
graph TD
subgraph Shortest path tree
t1((1)) --> t6((6))
t1((1)) --> t2((2))
t1((1)) --> t5((5))
t2((2)) --> t3((3))
t5((5)) --> t4((4))
end
subgraph Graph
g1((1)) --- g6((6))
g1((1)) --- g5((5))
g1((1)) --- g2((2))
g5((5)) --- g4((4))
g2((2)) --- g3((3))
g4((4)) --- g3((3))
end

```

Реализация BFS:

```python
from collections import deque

def bfs(graph: NodeGraph, start_node: int):
    q = deque()
    visited = [False] * graph.vertices_size
    visited[start_node] = True
    parent = [-1] * graph.vertices_size
    q.appendleft(start_node)

    while q:
        parent_index = q.pop()
        child = graph.edges[parent_index]
        while child:
            if not visited[child.vertex]:
                q.appendleft(child.vertex)
                parent[child.vertex] = parent_index
                visited[child.vertex] = True
            child = child.next_edge
    return parent
```

### Компоненты связности

> **Компонентой связности (connected component)** неориентированного графа называется максимальный набор его вершин, для которого существет путь между каждой парой вершин. Эти компоненты являются отдельными кусками графа, которые не соеденины между собой.

Компоненты связности можно найти с помощью обхода в ширину: Начинаем обход с первой вершины. Все элементы, обнаруженные в течение этого обхода являются членами одной компоненты связности. Затем повторяем обход с первой не обнаруженной вершины и т.д.

### Задача раскраски графа

> В задаче **раскраски графа (vertex colouring)** требуется присвоить метку или цвет каждой вершине графа таким образом, чтобы любые две соединённые ребром вершины были разного цвета. При этом нужно использовать как можно меньше цветов.

> Граф называется **двудольным (bipartite)**, если его вершины можно правильно раскрасить двумя цветами.

Определить, является ли граф двудольным можно расширив алгоритм обхода в ширину таким образом, чтобы раскрашивать каждую новую открытую вершину цветом, противоположенным цвету её предшественника. Потом проверить, не связывает ли два каких ребра вершины одинакового цвета.



























